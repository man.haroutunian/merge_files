files = ['file1.py', 'file2.py']
with open('merge_here.py', 'w') as newfile:
    for file_name in files:
        with open(file_name) as oldfiles:
            newfile.write(oldfiles.read())
